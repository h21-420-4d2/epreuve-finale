const OpenApiValidator = require('express-openapi-validator');

const { Router } = require('express');

const authMiddleware = require('../modules/auth-middleware');
const auth = require('./auth');
const users = require('./users');
const clients = require('./clients');
const inscriptions = require('./inscriptions');
const evenements = require('./evenements');

const router = Router();

router.use(
  OpenApiValidator.middleware({
    apiSpec: './specs/api.yaml',
    validateRequests: true, // (default)
    validateResponses: true, // false by default
  }),
);

router.use('/auth', auth);
router.use('/users', authMiddleware, users);
router.use('/clients', authMiddleware, clients);
router.use('/inscriptions', authMiddleware, inscriptions);
router.use('/evenements', authMiddleware, evenements);

module.exports = router;
